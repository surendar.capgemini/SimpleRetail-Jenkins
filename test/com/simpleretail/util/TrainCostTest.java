package com.simpleretail.util;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.retail.core.TrainMethodCostCalculation;
import com.retail.model.Item;
import com.retail.model.Item.ShippingMethod;

public class TrainCostTest {
	
	private TrainMethodCostCalculation testTrainCost;
	
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Before
	public void setUp() {
		testTrainCost = new TrainMethodCostCalculation();
	}

	@Test
	public void testCalculateCost() {
		assertEquals(10.0, testTrainCost.calculateCost(new Item(312321101516L, "Hot Tub", 9899.99f, 793.41f, ShippingMethod.RAIL)),.001);
	}
	
	@Test
	public void testCalculateCost1() {
		anException.expect(NullPointerException.class);
		throw new NullPointerException("Item is Null");
	}
}
