package com.retail.core;
import java.text.DecimalFormat;

import com.retail.model.Item;

public class AirMethodCostCalculation implements Calculation {
	static float cost = 0;

	//For calculation of shipping cost of items which have shipping method as AIR
	@Override
	public float calculateCost(Item item) {
		String x = item.getProductCode() + "";
		char digit = x.charAt(x.length() - 2);
		int p = Character.getNumericValue(digit);
		cost = (item.getProductWeight() * p) /*+ (((item.getProductWeight() * p) * 3) / 100)*/;
		cost = Float.parseFloat(new DecimalFormat("##.##").format(cost));
		return cost;
	}
}
