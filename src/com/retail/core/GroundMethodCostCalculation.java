package com.retail.core;
import java.text.DecimalFormat;

import com.retail.model.Item;

public class GroundMethodCostCalculation implements Calculation {
	static float cost = 0;

	//For calculation of shipping cost of items which have shipping method as GROUND

	@Override
	public float calculateCost(Item item) {
		cost = (float) (item.getProductWeight() * 2.5);
		cost = Float.parseFloat(new DecimalFormat("##.##").format(cost));
		return cost;
	}
}
