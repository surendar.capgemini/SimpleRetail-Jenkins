package com.retail.core;

import java.text.DecimalFormat;

import com.retail.model.Item;

public class TrainMethodCostCalculation implements Calculation {
	static float cost = 0;

	@Override
	public float calculateCost(Item item) {
		if(item.getProductWeight() < 5.0) 
			cost = 5;
		else
			cost = 10;
		cost = Float.parseFloat(new DecimalFormat("##.##").format(cost));
		return cost;
	}

}
