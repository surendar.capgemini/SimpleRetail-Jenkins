package com.retail.core;
import com.retail.model.Item;

public interface Calculation {
	float calculateCost(Item item);
}
