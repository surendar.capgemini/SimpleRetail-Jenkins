package com.retail.client;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.retail.core.AirMethodCostCalculation;
import com.retail.core.Calculation;
import com.retail.core.GroundMethodCostCalculation;
import com.retail.core.TrainMethodCostCalculation;
import com.retail.model.Item;
import com.retail.model.Item.ShippingMethod;
import com.retail.utils.UPCComparator;

public class ItemClient {
	private static Calculation calculation;

	public static void main(String[] args) {
		Item item1 = new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f,
				ShippingMethod.AIR);
		Item item2 = new Item(567321101986L, "CD - Beatles, Abbey Road", 17.99f, 0.61f, ShippingMethod.GROUND);
		Item item3 = new Item(567321101985L, "CD - Queen, A Night at the Opera", 20.49f, 0.55f, ShippingMethod.AIR);
		Item item4 = new Item(567321101984L, "CD - Michael Jackson, Thriller", 23.88f, 0.50f, ShippingMethod.GROUND);
		Item item5 = new Item(467321101899L, "iPhone - Waterproof Case", 9.75f, 0.73f, ShippingMethod.AIR);
		Item item6 = new Item(477321101878L, "iPhone -  Headphones", 17.25f, 3.21f, ShippingMethod.GROUND);
		Item item7 = new Item(312321101516L, "Hot Tub", 9899.99f, 793.41f, ShippingMethod.RAIL);
		Item item8 = new Item(322322202488L, "HeavyMac Laptop", 4555.79f, 4.08f, ShippingMethod.RAIL);

		Set<Item> itemsSet = new HashSet<Item>();
		itemsSet.add(item1);
		itemsSet.add(item2);
		itemsSet.add(item3);
		itemsSet.add(item4);
		itemsSet.add(item5);
		itemsSet.add(item6);
		itemsSet.add(item7);
		itemsSet.add(item8);

		List<Item> items = new ArrayList<>(itemsSet);
		Collections.sort(items, new UPCComparator());

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String shippingDate = formatter.format(date);

		System.out.printf("%-10s %50s\n\n", "****Shipping Report****", "Date:" + shippingDate + "\n");
		System.out.printf("%-15s%-40s%-12s%-12s%-12s%s\n\n", "UPC", "Description", "Price", "Weight", "Ship Method",
				"Shipping Cost");

		float airCost = 0, groundCost = 0, trainCost = 0, airSum = 0, groundSum = 0, trainSum = 0;
		for (Item item : items) {
			if ("AIR".equals(item.getShippingMethod().toString())) {
				calculation = new AirMethodCostCalculation();
				airCost = calculation.calculateCost(item);
				System.out.printf("%-15d%-40s%-12.2f%-12.2f%-12s%.2f\n", item.getProductCode(),
						item.getProductDescription(), item.getProductPrice(), item.getProductWeight(),
						item.getShippingMethod(), airCost);
				airSum = airSum + airCost;
			} else if ("GROUND".equals(item.getShippingMethod().toString())) {
				calculation = new GroundMethodCostCalculation();
				groundCost = calculation.calculateCost(item);
				System.out.printf("%-15d%-40s%-12.2f%-12.2f%-12s%.2f\n", item.getProductCode(),
						item.getProductDescription(), item.getProductPrice(), item.getProductWeight(),
						item.getShippingMethod(), groundCost);
				groundSum = groundSum + groundCost;
			} else if ("RAIL".equals(item.getShippingMethod().toString())) {
				calculation = new TrainMethodCostCalculation();
				trainCost = calculation.calculateCost(item);
				System.out.printf("%-15d%-40s%-12.2f%-12.2f%-12s%.2f\n", item.getProductCode(),
						item.getProductDescription(), item.getProductPrice(), item.getProductWeight(),
						item.getShippingMethod(), trainCost);
				trainSum = trainSum + trainCost;
			}
		}
		System.out.printf("\n%-91s%.2f", "TOTAL SHIPPING COST:", airSum + groundSum + trainSum);
	}
}